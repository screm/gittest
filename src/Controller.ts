import{AppError} from "./AppError"
import{Database} from "./Database"
import { FileOptions } from "./FileOptions";
import{Product} from "./Product"

export class Controller{
    private static db: Database;
    private static options: FileOptions;

    public constructor() {
        Controller.options = new FileOptions();
        Controller.options.root = "/Users/marcel/Documents/Schule/NVS/TypeScript/sourceVSCode/05REST2/c/wwwPublic";
        Controller.db = new Database();
    }
    public getAllProducts(req,res):void{
        let collFilteredProducts = Controller.db.getFilteredProducts(req.query.category, req.query.color);
        console.log("===== get all or filtered products");
        res.json(collFilteredProducts);
    }
    public getProductById(req,res,next):void{
        let product: Product;
        try{
            console.log("===== get product by id");
            product = Controller.db.getProductById(req.params.id);
            res.json(product);      
        }
        catch(error){
            let err: AppError = new AppError((error as Error).message);
            err.status = "fail";
            err.statusCode = 404;
            next(err);
        }     
    }
    public initProducts(req,res):void{
        Controller.db.initProducts();
        console.log("===== init done");
        res.json("init done");
    }
    public showErrorPage(req,res):void{
        console.log("===== error page ..." + req.originalUrl);
        res.status(404).json({
            status: "fail",
            message: "Can't find " + req.originalUrl + " on this server!"
        });
    }
    public doErrorHandling(err: AppError, req, res, next):void{
        console.log("===== error handling ..." + req.originalUrl + "..." + JSON.stringify(err));
        err.statusCode = err.statusCode || 404;
        err.status = err.status || "unknown error";
        res.status(err.statusCode).json(err);
    }
    public addProduct(req,res,next):void{
        try{          
            let newProduct: Product = Product.fromJson(req.body);
            newProduct = Controller.db.addProduct(newProduct);
            console.log("===== add product: " + newProduct.toString());
            res.json(newProduct);
        }
        catch(error){
            let appError: AppError = new AppError("error: " + (error as Error).message);
            appError.status = "fail";
            appError.statusCode = 400;
            next(appError);
        }
    }
    public updateProductById(req,res,next):void{
        try{
            let updateProduct: Product = Product.fromJson(req.body);
            updateProduct = Controller.db.updateProduct(updateProduct);
            console.log("===== update product: " + updateProduct.toString());
            res.json(updateProduct);
        }
        catch(error){
            let appError: AppError = new AppError("error: " + (error as Error).message);
            appError.status = "fail";
            appError.statusCode = 400;
            next(appError);
        }
    }
    public deleteProductById(req,res,next):void{
        try{
            const productId = req.params.id;
            Controller.db.deleteProduct(productId);
            res.status(204).end();
        }
        catch(error){
            let appError: AppError = new AppError("error: " + (error as Error).message);
            appError.status = 'fail';
            appError.statusCode = 400;
            next(appError);
        }
    }
    public showIndexPage(req,res):void{
        res.sendFile("html/index.html", Controller.options);
        console.log("===== show index.html");
    }
    public showImagesAndCSS(req,res):void{
        let url: string = req.originalUrl;
        console.log("===== show images/CSS... " + url);
        res.sendFile(url, Controller.options);
    }
}