import {Product} from "./Product";

export class Database{
    private static collProducts: Array<Product>;

    public constructor(){

    }
    public initProducts():void{
        Database.collProducts = new Array<Product>();
        try{
            Database.collProducts.push(new Product("Notebook", "tool", "black"));
            Database.collProducts.push(new Product("Monitor", "tool", "grey"));
            Database.collProducts.push(new Product("Borgward", "vehicle", "red"));
            Database.collProducts.push(new Product("Bike", "vehicle", "black"));
            Database.collProducts.push(new Product("Ski", "tool", "red"));
            Database.collProducts.push(new Product("Socks", "tool", "red"));
        } catch(error){
            console.log("===== unexpected error happened: " + (error as Error).message + " (" + (error as Error).name + ")");
        }
    }
    public getProductById(id: number):Product{
        let retProduct: Product = null;
        retProduct = Database.collProducts.find(product => product.getId() == id);
        if(retProduct == null){
            throw new Error("no product found with id: " + id);
        }
        return retProduct;
    }
    public getFilteredProducts(category:string, color:string){
        let paraProducts: Array<Product> = Database.collProducts;
            if(category != null && category.length > 0){
                paraProducts = paraProducts.filter(product => product.getCategory() == category);
            }
            if(color != null && color.length > 0){
                paraProducts = paraProducts.filter(product => product.getColor() == color);
            }
        return paraProducts;
    }
    public addProduct(p: Product):Product{
        let retProduct: Product = new Product(p.getName(), p.getCategory(), p.getColor());
        Database.collProducts.push(retProduct);
        return retProduct;
    }
    public updateProduct(p:Product):Product{
        let index = Database.collProducts.findIndex((product) => product.getId() === p.getId());

        if (index < 0) {
            throw new Error(`Product with ID ${p.getId()} not found`);
        }

        Database.collProducts[index].setName(p.getName());
        Database.collProducts[index].setCategory(p.getCategory());
        Database.collProducts[index].setColor(p.getColor());

        return Database.collProducts[index];
    }
    public deleteProduct(id:number):Product{
        let product: Product = null;
        try{
            product = this.getProductById(id);
            let index: number = Database.collProducts.indexOf(product)
            Database.collProducts.splice(index, 1);
        }catch(error){
            console.log("===== unexpected error happened: " + (error as Error).message + " (" + (error as Error).name + ")");
            throw new Error("ERROR: " + (error as Error).message);
        }
        return product;
    }
}