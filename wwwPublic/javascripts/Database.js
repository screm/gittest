"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Database = void 0;
const Product_1 = require("./Product");
class Database {
    constructor() {
    }
    initProducts() {
        Database.collProducts = new Array();
        try {
            Database.collProducts.push(new Product_1.Product("Notebook", "tool", "black"));
            Database.collProducts.push(new Product_1.Product("Monitor", "tool", "grey"));
            Database.collProducts.push(new Product_1.Product("Borgward", "vehicle", "red"));
            Database.collProducts.push(new Product_1.Product("Bike", "vehicle", "black"));
            Database.collProducts.push(new Product_1.Product("Ski", "tool", "red"));
            Database.collProducts.push(new Product_1.Product("Socks", "tool", "red"));
        }
        catch (error) {
            console.log("===== unexpected error happened: " + error.message + " (" + error.name + ")");
        }
    }
    getProductById(id) {
        let retProduct = null;
        retProduct = Database.collProducts.find(product => product.getId() == id);
        if (retProduct == null) {
            throw new Error("no product found with id: " + id);
        }
        return retProduct;
    }
    getFilteredProducts(category, color) {
        let paraProducts = Database.collProducts;
        if (category != null && category.length > 0) {
            paraProducts = paraProducts.filter(product => product.getCategory() == category);
        }
        if (color != null && color.length > 0) {
            paraProducts = paraProducts.filter(product => product.getColor() == color);
        }
        return paraProducts;
    }
    addProduct(p) {
        let retProduct = new Product_1.Product(p.getName(), p.getCategory(), p.getColor());
        Database.collProducts.push(retProduct);
        return retProduct;
    }
    updateProduct(p) {
        let index = Database.collProducts.findIndex((product) => product.getId() === p.getId());
        if (index < 0) {
            throw new Error(`Product with ID ${p.getId()} not found`);
        }
        Database.collProducts[index].setName(p.getName());
        Database.collProducts[index].setCategory(p.getCategory());
        Database.collProducts[index].setColor(p.getColor());
        return Database.collProducts[index];
    }
    deleteProduct(id) {
        let product = null;
        try {
            product = this.getProductById(id);
            let index = Database.collProducts.indexOf(product);
            Database.collProducts.splice(index, 1);
        }
        catch (error) {
            console.log("===== unexpected error happened: " + error.message + " (" + error.name + ")");
            throw new Error("ERROR: " + error.message);
        }
        return product;
    }
}
exports.Database = Database;
//# sourceMappingURL=Database.js.map