export class Product {
    constructor(name, category, color) {
        this.id = Product.lastId;
        Product.lastId++;
        this.name = name;
        this.category = category;
        this.color = color;
    }
    getId() {
        return this.id;
    }
    getName() {
        return this.name;
    }
    getCategory() {
        return this.category;
    }
    getColor() {
        return this.color;
    }
    toString() {
        return "Product: [" + this.id + ", " + this.name + ", " + this.category + ", " + this.color + "]";
    }
    static fromJson(data) {
        let retProduct = new Product(data.name, data.category, data.color);
        retProduct.id = data.id;
        return retProduct;
    }
    static setFilterId(id) {
        Product.filterId = id;
    }
    setName(name) {
        this.name = name;
    }
    setCategory(category) {
        this.category = category;
    }
    setColor(color) {
        this.color = color;
    }
    setId(id) {
        this.id = id;
    }
    static filterByCatAndColor(filterProdcut) {
        let retProductOK = false;
        if (Product.filterCategory != null && Product.filterColor != null && Product.filterCategory == filterProdcut.category && Product.filterColor == filterProdcut.color) {
            retProductOK = true;
        }
        else {
            if (Product.filterCategory != null && Product.filterColor != null && Product.filterCategory == filterProdcut.category) {
                retProductOK = true;
            }
            else {
                if (Product.filterColor != null && Product.filterCategory == null && Product.filterColor == filterProdcut.color) {
                    retProductOK = true;
                }
            }
        }
        return retProductOK;
    }
}
Product.lastId = 1;
Product.filterId = 1;
//# sourceMappingURL=Product.js.map